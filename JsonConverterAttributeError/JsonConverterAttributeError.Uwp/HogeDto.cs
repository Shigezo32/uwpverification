﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace JsonConverterAttributeError
{
    public class HogeDto
    {
        [JsonConverter(typeof(HogeJsonConverter))]
        public int Hoge { get; set; }
    }
}
